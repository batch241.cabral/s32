const http = require("http");

const port = 8080;

const users = [
    {
        "name": "Jose Marie Chan",
        "email": "jmarie@gmail.com",
    },
    {
        "name": "Mariah Carey",
        "email": "mariahcarey@gmail.com",
    },
];
const server = http.createServer(function (req, res) {
    if (req.url == "/users" && req.method == "GET") {
        res.writeHead(200, { "Content-type": "application/json" });
        res.write(JSON.stringify(users));
        res.end();
    } else if (req.url == "/users" && req.method == "POST") {
        let req_body = "";
        req.on("data", function (data) {
            req_body += data;
            console.log(req_body);
        });
        req.on("end", function () {
            console.log(typeof req_body);
            req_body = JSON.parse(req_body);

            let new_user = {
                "name": req_body.name,
                "email": req_body.email,
            };
            users.push(new_user);
            console.log(users);

            res.writeHead(200, { "Content-type": "application/json" });
            res.write(JSON.stringify(new_user));
            res.end();
        });
    }
});

server.listen(port);

console.log(`Server is running at localhost: ${port}`);
