const http = require("http");
const port = 3000;

const server = http.createServer(function (req, res) {
    if (req.url == "/items" && req.method == "GET") {
        res.writeHead(200, { "Content-type": "text/plain" });
        res.end("Data Retrieved from the database");
    } else if (req.url == "/items" && req.method == "POST") {
        res.writeHead(200, { "Content-type": "text/plain" });
        res.end("Data was created and was sent to database");
    }
});
server.listen(port);

console.log(`Server is now accessible at localhost: ${port}`);
